<!DOCTYPE html>
<html>
  <head>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="{{asset('css/materialize.min.css')}}"  media="screen,projection"/>
    <meta charset="utf-8">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>


    <style type="text/css">
@font-face {
font-family: 'Lato';
font-style: normal;
src: local('Lato Regular'), local('Lato-Regular'), url(font/IRANSansWeb_Light.woff2) format('woff2');
}
/* latin */
@font-face {
font-family: 'Lato';
font-style: normal;
src: local('Lato Regular'), local('Lato-Regular'), url(font/IRANSansWeb_Light.woff2) format('woff2');
}
  html{
    height: 100%;
  }
  body{
    height: 100%;
    font-family: 'Lato', sans-serif;
    background: blue;
    background: -webkit-linear-gradient(right bottom, blue, white, green);
    background: -o-linear-gradient(bottom left, blue, white, green);
    background: -moz-linear-gradient(bottom left, blue, white, green);
    background: linear-gradient(to bottom left, blue, white, green);
    background-size: cover;
    background-position: 50% 50%;
    background-repeat: none;
    background-attachment: fixed;
  }
  *{
    font-family: 'Lato', sans-serif;
  }
      .brand-logo{
        margin-left:20px;
      }
      .parallax img{
        /*filter: blur(5px);*/
      }
      main{
        position: relative;
        top:-64px;
      }
      nav{
        transition: 0.3s;
      }
      nav.no-shadow{
        box-shadow: none;
      }
      #index-banner{
        z-index: 1;
        height: auto;
      }
      #index-banner > .section{
        width:100%;
      }
      #index-banner h1.header{
        text-shadow: 0px 7px 15px black;
      }
      .nav-wrapper a.brand-logo{
        font-size: 1.7rem;
        margin-top: -10px;
        margin-right: 8px;
      }
      nav{
        height: 80px;
        background: linear-gradient(#555, transparent);
      }
      /*.nav-wrapper a.brand-logo, .nav-wrapper span{
        text-shadow: 0px 0px 10px black;
      }*/
    </style>
  </head>

  <body>

  <div class="navbar-fixed">
    <nav class="transparent no-shadow">
  <div class="nav-wrapper">
    <a href="#!" class="brand-logo right valign-wrapper"><img src="imam-1.jpg" class="right align" width="70" height="70" style="margin-top:13px;margin-right:-10px;border-radius:50%;"/>ستاد اسکان</a>
    <span class="right" style="margin-top:22px;margin-right:85px;">خیرین شجره طیبه</span>
    <ul class="left hide-on-med-and-down">
      <li class="waves-effect waves-light"><a><i class="material-icons">person</i></a></li>
      <li class="waves-effect waves-light"><a href="#"><i class="material-icons">info</i></a></li>
      <li class="waves-effect waves-light"><a href="#"><i class="material-icons">help</i></a></li>
    </ul>
  </div>
</nav>
</div>


<main>
<div id="index-banner" class="parallax-container">
  <div class="section no-pad-bot valign">
    <div class="container" style="position: relative; top: 30px;">
      <br><br>
      <h1 class="header center black-text">سامانه ثبت نام و رزرو</h1>
      <div class="row center">
        <br><br>
        <a href="{{url('rules')}}" id="download-button" class="btn-large waves-effect waves-light orange lighten-1">ورود</a>
      </div>
      <br><br>

    </div>
  </div>
  <!--div class="parallax"><img src="bg-slider-1.jpg" alt="Unsplashed background img 1"></div-->
</div>


<!--style type="text/css">
  .cb-slideshow,
.cb-slideshow:after { 
    position: fixed;
    width: 100%;
    height: 100%;
    top: 0px;
    left: 0px;
    z-index: 0; 
}


.cb-slideshow li span { 
    width: 100%;
    height: 100%;
    position: absolute;
    top: -15px;
    left: 0px;
    color: transparent;
    background-size: cover;
    background-position: 50% 50%;
    background-repeat: none;
    opacity: 0;
    z-index: 0;
    animation: imageAnimation 34s linear infinite 0s;
}



.cb-slideshow li:nth-child(1) span { 
    background-image: url(bg-slider-4.jpg) 
}
.cb-slideshow li:nth-child(2) span { 
    background-image: url(bg-slider-6.jpg);
    animation-delay: 6s;
}
.cb-slideshow li:nth-child(3) span { 
    background-image: url(bg-slider-4.jpg);
    animation-delay: 12s; 
}
.cb-slideshow li:nth-child(4) span { 
    background-image: url(bg-slider-6.jpg);
    animation-delay: 24s;
}


@keyframes imageAnimation { 
    0% { opacity: 0; animation-timing-function: ease-in; }
    8% { opacity: 1; animation-timing-function: ease-out; }
    17% { opacity: 1 }
    25% { opacity: 0 }
    100% { opacity: 0 }
}

.no-cssanimations .cb-slideshow li span{
  opacity: 1;
}
</style>

<ul class="cb-slideshow">
  <li>
    <span></span>
  </li>
  <li>
    <span></span>
  </li>
  <li>
    <span></span>
  </li>
  <li>
    <span></span>
  </li>
</ul!-->



</main>


    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="{{asset('jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/materialize.js')}}"></script>
    <script type="text/javascript">
    $(function(){
      //$('#index-banner').height($(window).height());
      $('.button-collapse').sideNav();
      $('.parallax').parallax();
    })(jQuery);
    </script>
  </body>
</html>
