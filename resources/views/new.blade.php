<?php
use DB as DB;
$cities = DB::table('cities')->get();
?>
<!DOCTYPE html>
<html>
  <head>
    <title>ستاد اسکان ندسا</title>
    <link rel="icon" type="image/jpeg" href="/imam-1.jpg">
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="{{asset('css/materialize.css')}}"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="{{asset('css/sweetalert.css')}}"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="{{asset('css/persian-datepicker.min.css')}}"  media="screen,projection"/>
    <meta charset="utf-8">
    <!--Let browser know website is optimized for mobile-->
    <!--meta name="viewport" content="width=device-width, initial-scale=1.0"/-->


    <style type="text/css">
@font-face {
font-family: 'Lato';
font-style: normal;
src: local('Lato Regular'), local('Lato-Regular'), url(font/IRANSansWeb_Light.woff2) format('woff2');
}
/* latin */
@font-face {
font-family: 'Lato';
font-style: normal;
src: local('Lato Regular'), local('Lato-Regular'), url(font/IRANSansWeb_Light.woff2) format('woff2');
}
  html{
    height: 100%;
  }
  body{
    height: 100%;
    font-family: 'Lato', sans-serif;
    background: blue;
    background: -webkit-linear-gradient(right bottom, blue, white, green);
    background: -o-linear-gradient(bottom left, blue, white, green);
    background: -moz-linear-gradient(bottom left, blue, white, green);
    background: linear-gradient(to bottom left, blue, white, green);
    background-size: cover;
    background-position: 50% 50%;
    background-attachment: fixed;
  }
  *{
    font-family: 'Lato', sans-serif;
  }
      .brand-logo{
        margin-left:20px;
      }
      .parallax img{
        /*filter: blur(5px);*/
      }
      main{
        position: relative;
        top:-64px;
      }
      nav{
        transition: 0.3s;
      }
      nav.no-shadow{
        box-shadow: none;
      }
      #index-banner{
        z-index: 0;
        height: auto !important;
        overflow-y: hidden;
      }
      #index-banner > .section{
        width:100%;
        margin-top: 70px;
      }
      #index-banner h1.header{
        text-shadow: 0px 7px 15px black;
      }

      .nav-wrapper a.brand-logo{
        font-size: 1.7rem;
        margin-top: -10px;
        margin-right: 8px;
      }
      nav{
        height: 80px;
        background: linear-gradient(#555, transparent);
      }
      .main-card{
        border-top: 4px purple solid;
      }


      .dropdown-content{
        height: 300px;
      }
    </style>
  </head>

  <body>

  <div class="navbar-fixed">
    <nav class="transparent no-shadow">
  <div class="nav-wrapper">
    <a href="#!" class="brand-logo right valign-wrapper"><img src="imam-1.jpg" class="right align" width="70" height="70" style="margin-top:13px;margin-right:-10px;border-radius:50%;"/>ستاد اسکان</a>
    <span class="right" style="margin-top:22px;margin-right:85px;">خیرین شجره طیبه</span>
    <ul class="left hide-on-med-and-down">
      <li class="waves-effect waves-light"><a><i class="material-icons">person</i></a></li>
      <li class="waves-effect waves-light"><a href="#"><i class="material-icons">info</i></a></li>
      <li class="waves-effect waves-light"><a href="#"><i class="material-icons">help</i></a></li>
    </ul>
  </div>
</nav>
</div>



<main>
<div id="index-banner" class="parallax-container">
  <div class="section no-pad-bot valign">
    <div class="container">
    <div class="main-card" style="width:100%;border-radius:3px 3px 0px 0px;margin-bottom:-8px;z-index:999;position:relative;"></div>

















        <div class="row" id="main-info">
<div class="col s12 m12 l12">
<div class="card" id="first-card" style="display:none;">
<div class="card-content">









  <div class="row" dir="rtl">
    <form class="col s12 m12 l12">
      <div class="row">
        <div class="input-field col s6 right">
            <select onchange="loadPlaces(this)" id="list-city">
              <option value="0" disabled selected>شهر خود را انتخاب کنید</option>
              @foreach ($cities as $city)
              <option value="{{$city->id}}">  {{$city->title}}  </option>
              @endforeach
            </select>
            <label class="right">انتخاب شهر</label>
        </div>
        <div class="input-field col s6">
            <select id="list-places" disabled id="list-place">
              <option value="0" disabled selected>محل اسکان را انتخاب کنید</option>
              <option class="m-m" value="1">Option 1</option>
              <option class="m-m" value="2">Option 2</option>
              <option class="m-m" value="3">Option 3</option>
            </select>
            <label class="right">محل اسکان</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s3" style="margin-left: 3%;margin-top:0px;width: 22% !important;">
          <input type="checkbox" id="filled-in-box"/>
          <label for="filled-in-box" class="left">رزرو به صورت دربست</label>


          <br>
          <div style="display: block;height: 10px;"></div>
          <input type="checkbox" id="req-food" style="position:relative;" />
          <label for="req-food" class="left">رزرو با غذا</label>
        </div>
        <div class="input-field col s3">
          <input id="persons" type="text">
          <label for="persons">تعداد نفرات</label>
        </div>
        <div class="input-field col s3">
        <div style="position:absolute;left:10px;color:white;padding:5px 10px;top: 30px;border-radius: 0px 0px 5px 5px;" class="blue z-depth-3">
        <span id="date-duration">۰</span> روز
        </div>
          <input id="date-until" type="text">
          <label for="date-until">تا تاریخ</label>
        </div>
        <div class="input-field col s3">
          <input id="date-from" type="text">
          <label for="date-from">از تاریخ</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12 center-align">
          <a class="waves-effect waves-light btn blue btn-large" style="z-index:0;" onclick="loadResults()"><i class="material-icons right">search</i>جستجو</a>
        </div>
      </div>
    </form>
  </div>





</div>
</div>
</div>
</div>







<div class="row" id="results" style="display: none;">
<div class="col s12 m12 l12">
<div class="card gray">

<div class="card-content">









<div class="row" dir="rtl" style="border-bottom: 1px gray solid;" id="results-item">
  <span class="not-reserved">
    <span class="name purple-text">YYYY-MM-DD</span>
    با تعرفه ی به ازای هر نفر در هر روز
    <span class="price blue-text">yyyyyy</span>
    تومان
  </span>

  <span class="full-reserved" style="display: none;">
    <span class="name purple-text">YYYY-MM-DD</span>
    با تعرفه به ازای رزرو دربست
    <span class="price blue-text">yyyyyy</span>
    تومان
  </span>
  <button class="waves-effect waves-light btn green left" style="position:relative;top:-11px;"><i class="material-icons right">star</i>تایید</button>
</div>







</div>
</div>
</div>
</div>




<!--div class="row">
  <div class="col s4 offset-s4">
    <div class="card hoverable sticky-action">
    <div class="card-image waves-effect waves-block waves-light">
      <img class="activator" src="bg1.jpg">
    </div>
    <div class="card-content right-align" dir="rtl">
      <span class="card-title activator grey-text text-darken-4">Card Title<i class="material-icons left">more_vert</i></span>
      <p><a href="#">This is a link</a></p>
    </div>
    <div class="card-reveal">
      <span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
      <p>Here is some more information about this product that is only revealed once clicked on.</p>
    </div>
    <div class="card-action">
      <a href="#" class="purple-text">تایید</a>
    </div>
  </div>
  </div>
</div-->





















<div class="row" id="form-add" style="display:none;">
<div class="col s12 m12 l12">
<div class="card">
<div class="card-content">









<div class="row" dir="rtl">
  <div class="row"><h4 style="width: 100%;text-align:center;">اطلاعات سرپرست</h4></div>
<form class="col s12 m12 l12">
  <div class="row">
    <div class="input-field col s3">
      <select id="person-status">
        <option value="0" selected>انتخاب</option>
        <option class="m-m" value="1">پاسدار</option>
        <option class="m-m" value="2">غیر پاسدار</option>
      </select>
      <label class="right">وضعیت<i style="color:red;padding-right:5px;margin-bottom:-5px;">*</i></label>
    </div>
    <div class="input-field col s3">
      <input class="validate" id="parent-phone" type="text" required="true" aria-required="true">
      <label for="parent-phone">شماره تلفن <i class="red-text">*</i></label>
    </div>
    <div class="input-field col s3">
      <input class="validate" id="code-melli" type="text" required="true" aria-required="true">
      <label for="code-melli">کد ملی <i class="red-text">*</i></label>
    </div>
    <div class="input-field col s3">
      <input class="validate" id="parent-name" type="text" required="true" aria-required="true">
      <label for="parent-name">نام <i class="red-text">*</i></label>
    </div>
  </div>
</form>


<div id="followers">

  <div class="row"><h4 style="width: 100%;text-align:center;">اطلاعات همراهان</h4></div>
<form class="col s12 m12 l12">
  <div class="row">
    <div class="input-field col s4">
      <input id="f_parent-phone" class="parent-phone" type="text">
      <label for="f_parent-phone">شماره تلفن</label>
    </div>
    <div class="input-field col s4">
      <input id="f_code-melli" class="parent-code" type="text">
      <label for="f_code-melli">کد ملی</label>
    </div>
    <div class="input-field col s4">
      <input id="f_parent-name" class="parent-name" type="text">
      <label for="f_parent-name">نام</label>
    </div>
  </div>
</form>


</div>
</div>
<div class="row" style="position: relative;top: -20px;top:0px;left:0px;">
    <button class="waves-effect waves-light btn orange left" onclick="newFollowerForm()"><i class="material-icons right">add</i>افزودن</button>
</div>
<div class="row center-align" style="position: relative;top: -20px;top:0px;left:0px;">
    <button class="waves-effect waves-light btn green btn-large" onclick="showPrompt()"><i class="material-icons right" style="transform:rotate(180deg);">send</i>ثبت</button>
    <button class="waves-effect waves-light btn red btn-large" onclick="backTo()">بازگشت</button>
</div>



</div>
</div>
</div>
</div>











    </div>
  </div>
  <!--div class="parallax"><img src="bg2.jpg" alt="Unsplashed background img 1"></div-->
</div>



  <div id="modal1" class="modal">
    <div class="modal-content" style="text-align:right;">
      <h4>درخواست شما ثبت شد</h4>
      <p>کد پیگیری به شماره تلفن سرپرست ارسال خوهد شد!</p>
    </div>
    <div class="modal-footer">
      <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">برگشت</a>
    </div>
  </div>


  <div id="modal-promp" class="modal">
    <div class="modal-content" style="text-align:right;">
      <h4>آیا مطمئن به ثبت درخواست هستید؟</h4>
      <p>رزرو <span id="hotel-name"></span> برای <span id="hotel-persons"></span> نفر از تاریخ <span id="hotel-from"></span> تا تاریخ <span id="hotel-to"></span></p>
    </div>
    <div class="modal-footer">
    ‍  <a onclick="showShopLevel()" class=" modal-action modal-close waves-effect waves-white btn-flat green" style="margin-left:20px;">ثبت</a>
      <a href="#!" class=" modal-action modal-close waves-effect waves-white btn-flat red">برگشت</a>
    </div>
  </div>



  <div id="modal-watcher" class="modal">
    <div class="modal-content" style="text-align:right;" dir="rtl">
      <h4 class="center-align">جزیات سفارش</h4>
      <div class="row" style="margin-bottom: 0px !important;"><p class="col s12 m12 l12">محل اسکان: <span id="hotel-name">fff</span></p></div>
      <div class="row">
      <p class="col s4 m4 l4">تعداد افراد: <span id="hotel-persons"></span></p>
      <p class="col s4 m4 l4">تا تاریخ: <span id="hotel-to"></span></p>
      <p class="col s4 m4 l4">از تاریخ: <span id="hotel-from"></span></p>
      </div>
      <h5 class="center-align">اطلاعات سرپرست</h5>
      <table>
        <thead>
          <tr>
              <th data-field="id" class="right-align">نام</th>
              <th data-field="name" class="right-align">کد ملی</th>
              <th data-field="price" class="right-align">شماره تلفن</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td id="p-parent-name" class="right-align"></td>
            <td id="p-parent-code" class="right-align"></td>
            <td id="p-parent-phone" class="right-align"></td>
          </tr>
        </tbody>
      </table>
      <h5 class="center-align">اطلاعات همراهان</h5>
      <table>
        <thead>
          <tr>
              <th data-field="id" class="right-align">نام</th>
              <th data-field="name" class="right-align">کد ملی</th>
              <th data-field="price" class="right-align">شماره تلفن</th>
          </tr>
        </thead>

        <tbody id="p-followers">
        </tbody>
      </table>
      <div class="row" style="margin-bottom: 0px !important;"><p class="col s12 m12 l12"><h6><b>مبلغ قابل پرداخت: <span id="hotel-price">fff</span> تومان</p></b></h6></div>
    </div>
    <div class="modal-footer">
    ‍  <a onclick="sendData()" class=" modal-action modal-close waves-effect waves-white btn-flat green" style="margin-left:10px;"><i class="material-icons right">shop</i> پرداخت</a>
      <a href="#!" class=" modal-action modal-close waves-effect waves-white btn-flat red" style="margin-left:10px;">برگشت</a>

      <a href="#!" class=" modal-action modal-close waves-effect waves-white btn-flat blue" onclick="$('#modal-watcher .modal-content').printDiv();">چاپ</a>
    </div>
  </div>



</main>


<div id="loader" style="position:fixed;top:0px;width:100%;height:100%;background:rgba(0, 0, 0, 0.5);z-index:9999;display:none;">
<div class="sk-fading-circle">
  <div class="sk-circle1 sk-circle"></div>
  <div class="sk-circle2 sk-circle"></div>
  <div class="sk-circle3 sk-circle"></div>
  <div class="sk-circle4 sk-circle"></div>
  <div class="sk-circle5 sk-circle"></div>
  <div class="sk-circle6 sk-circle"></div>
  <div class="sk-circle7 sk-circle"></div>
  <div class="sk-circle8 sk-circle"></div>
  <div class="sk-circle9 sk-circle"></div>
  <div class="sk-circle10 sk-circle"></div>
  <div class="sk-circle11 sk-circle"></div>
  <div class="sk-circle12 sk-circle"></div>
</div>
</div>

<style type="text/css">
.sk-fading-circle {
  margin: 25% auto;
  width: 80px;
  height: 80px;
  position: relative;
}

.sk-fading-circle .sk-circle {
  width: 100%;
  height: 100%;
  position: absolute;
  left: 0;
  top: 0;
}

.sk-fading-circle .sk-circle:before {
  content: '';
  display: block;
  margin: 0 auto;
  width: 15%;
  height: 15%;
  background-color: white;
  border-radius: 100%;
  -webkit-animation: sk-circleFadeDelay 1.2s infinite ease-in-out both;
          animation: sk-circleFadeDelay 1.2s infinite ease-in-out both;
}
.sk-fading-circle .sk-circle2 {
  -webkit-transform: rotate(30deg);
      -ms-transform: rotate(30deg);
          transform: rotate(30deg);
}
.sk-fading-circle .sk-circle3 {
  -webkit-transform: rotate(60deg);
      -ms-transform: rotate(60deg);
          transform: rotate(60deg);
}
.sk-fading-circle .sk-circle4 {
  -webkit-transform: rotate(90deg);
      -ms-transform: rotate(90deg);
          transform: rotate(90deg);
}
.sk-fading-circle .sk-circle5 {
  -webkit-transform: rotate(120deg);
      -ms-transform: rotate(120deg);
          transform: rotate(120deg);
}
.sk-fading-circle .sk-circle6 {
  -webkit-transform: rotate(150deg);
      -ms-transform: rotate(150deg);
          transform: rotate(150deg);
}
.sk-fading-circle .sk-circle7 {
  -webkit-transform: rotate(180deg);
      -ms-transform: rotate(180deg);
          transform: rotate(180deg);
}
.sk-fading-circle .sk-circle8 {
  -webkit-transform: rotate(210deg);
      -ms-transform: rotate(210deg);
          transform: rotate(210deg);
}
.sk-fading-circle .sk-circle9 {
  -webkit-transform: rotate(240deg);
      -ms-transform: rotate(240deg);
          transform: rotate(240deg);
}
.sk-fading-circle .sk-circle10 {
  -webkit-transform: rotate(270deg);
      -ms-transform: rotate(270deg);
          transform: rotate(270deg);
}
.sk-fading-circle .sk-circle11 {
  -webkit-transform: rotate(300deg);
      -ms-transform: rotate(300deg);
          transform: rotate(300deg); 
}
.sk-fading-circle .sk-circle12 {
  -webkit-transform: rotate(330deg);
      -ms-transform: rotate(330deg);
          transform: rotate(330deg); 
}
.sk-fading-circle .sk-circle2:before {
  -webkit-animation-delay: -1.1s;
          animation-delay: -1.1s; 
}
.sk-fading-circle .sk-circle3:before {
  -webkit-animation-delay: -1s;
          animation-delay: -1s; 
}
.sk-fading-circle .sk-circle4:before {
  -webkit-animation-delay: -0.9s;
          animation-delay: -0.9s; 
}
.sk-fading-circle .sk-circle5:before {
  -webkit-animation-delay: -0.8s;
          animation-delay: -0.8s; 
}
.sk-fading-circle .sk-circle6:before {
  -webkit-animation-delay: -0.7s;
          animation-delay: -0.7s; 
}
.sk-fading-circle .sk-circle7:before {
  -webkit-animation-delay: -0.6s;
          animation-delay: -0.6s; 
}
.sk-fading-circle .sk-circle8:before {
  -webkit-animation-delay: -0.5s;
          animation-delay: -0.5s; 
}
.sk-fading-circle .sk-circle9:before {
  -webkit-animation-delay: -0.4s;
          animation-delay: -0.4s;
}
.sk-fading-circle .sk-circle10:before {
  -webkit-animation-delay: -0.3s;
          animation-delay: -0.3s;
}
.sk-fading-circle .sk-circle11:before {
  -webkit-animation-delay: -0.2s;
          animation-delay: -0.2s;
}
.sk-fading-circle .sk-circle12:before {
  -webkit-animation-delay: -0.1s;
          animation-delay: -0.1s;
}

@-webkit-keyframes sk-circleFadeDelay {
  0%, 39%, 100% { opacity: 0; }
  40% { opacity: 1; }
}

@keyframes sk-circleFadeDelay {
  0%, 39%, 100% { opacity: 0; }
  40% { opacity: 1; } 
}
</style>

<input type="hidden" id="f_place">
<input type="hidden" id="f_place_title">
<input type="hidden" id="f_nums" value="1">

    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="{{asset('jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/materialize.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/sweetalert.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/persian-date.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/persian-datepicker.min.js')}}"></script>
    <script type="text/javascript">
    window.timefrom = null;
    window.timeto = null;
    window.timeduration = null;
    $(function(){
      window.setInterval(function(){
        $("#first-card").slideDown();
      }, 1000);
      $('.modal').modal();
      //$('#index-banner').height($(window).height());
      $('.button-collapse').sideNav();
      $('.parallax').parallax();
      $('select').material_select();
      e1 = {
        format: 'YYYY-MM-DD',
        onSelect: function(unix){
          window.timefrom = unix;
          $("#date-until").pDatepicker({
            maxDate: unix,
            format: 'YYYY-MM-DD',
            onSelect: function(unix1){
              window.timeto = unix1;
              a = "" + Math.round((window.timeto - window.timefrom) / 1000 / (24 * 3600));
              window.timeduration = parseInt(a);
              $("#date-duration").html(toPersDigits(a));
            }
          }).pDatepicker("setDate",unix + (24 * 60 * 60 * 1000) );
        }
      };
      $("#date-from").pDatepicker(e1).val(null);
      $.fn.enableSelect = function(){
        $(this).material_select('destroy');
        $(this).prop('disabled', false);
        $(this).material_select();
        return this;
      }
      $.fn.disableSelect = function(){
        $(this).material_select('destroy');
        $(this).prop('disabled', true);
        $(this).material_select();
        return this;
      }
      $.fn.invalidateSelect = function(){
        $(this).material_select('destroy');
        $(this).material_select();
        return this;
      }



      $("input#filled-in-box").change(function(){
        if ($(this).is(":checked")){
          $("input#persons").prop('disabled', true).val(null);
        }else{
          $("input#persons").prop('disabled', false);
        }
      });




      $.fn.printDiv = function(){
        var divToPrint= $(this);
        var newWin = window.open('','جزیات');
        newWin.document.open();
        newWin.document.write('<html><head>' + $('head').html() + '</head><body onload="window.print()" dir="rtl" style="margin-left:20%;margin-right:20%;border:3px black solid;padding:20px;">' + divToPrint.html() + '</body></html>');
        newWin.document.close();
        setTimeout(function(){newWin.close();},500);
      }



      /*window.setTimeout(function(){
        $("#modal-watcher .modal-content").printDiv();ئ
      }, 1000);*/
    })(jQuery);
    function loadPlaces(e){
      url = '{{url('/')}}' + '/api/cities/' + $(e).val() + '/places';
      $.ajax({
        url: url
      }).done(function(e){
        if (e.length > 0){
          iv = $("#list-places").find('.m-m').first();
          $("#list-places").enableSelect().find('.m-m').remove();
          e.forEach(function(ele){
            $("#list-places").append(iv.clone().html(ele.title).attr('id', ele.id).val(ele.id));
          });
          $("#list-places").invalidateSelect();
        }else{
          swal({   title: "هیچ مکانی یافت نشد",   type: "error",   confirmButtonText: "تایید" });
          $("#list-places").disableSelect();
        }
      });
    }



    function boolTOstring(bool){
      if (bool){
        return "1";
      }else{
        return "0";
      }
    }

    function loadResults(){
      validated = false;
      if ($('#persons').val().trim() == "" && !$("input#filled-in-box").is(":checked"))
        validated = true;

      if ($('#date-until').val().trim() == "")
        validated = true;

      if ($('#date-from').val().trim() == "")
        validated = true;

      if ($('select#list-city').val() == "0")
        validated = true;

      if ($('select#list-place').val() == "0")
        validated = true;


      if (validated){
        swal({   title: "لطفا فیلد ها به صورت کامل پر شود",   type: "error",   confirmButtonText: "تایید" });
        return;
      }
      url = '{{url('/')}}' + '/api/places/get?reserv=' + boolTOstring($("input#filled-in-box").is(":checked")) + '&food=' + boolTOstring($("input#req-food").is(":checked")) + '&persons=' + $('#persons').val() + "&place=" + $('#list-places').val();
      $.ajax({
        url: url
      }).done(function(e){
        console.log(e);
        if (e.length > 0){
          iv = $("#results").find('#results-item').first();
          if ($("input#filled-in-box").is(":checked")) {
            iv.find(".not-reserved").remove();
            iv.find(".full-reserved").css('display', 'inline');
          }
          e.forEach(function(ele){
            iv.find('.name').html(ele.title);
            iv.find('.price').html(ele.price);
            iv.find('button').attr('place-id', ele.id);
            cont = iv.clone();
            $("#results").find('.card-content').append(cont);
            cont.find('button').click(function(event) {
              selectPlace(this);
            });
          });
          iv.remove();
          $('#results').fadeIn();
        }else{
          swal({   title: "هیچ مکانی یافت نشد",   type: "error",   confirmButtonText: "تایید" });
          $('#results').fadeOut();
        }
      });
    }




    function selectPlace(e){
      t = $(e);
      $("#f_place").val(t.attr('place-id'));
      $("#main-info").slideUp();
      $("#results").fadeOut();
      $("#form-add").slideDown();
      $("#f_place_title").val(t.prev().html());
    }
    
    
    
    function backTo(){
      $("#main-info").slideDown();
      $("#results").fadeIn();
      $("#form-add").slideUp();
    }




    function newFollowerForm(){
      console.log($('#persons').val() == $("#f_nums").val());
      if ((parseInt($('#persons').val()) - 1) == parseInt($("#f_nums").val())){
        swal({   title: "مجاز به افزودن مورد جدید نمی باشید",   type: "error",   confirmButtonText: "تایید" });
        return;
      }
      $("#f_nums").val("" + (parseInt($("#f_nums").val()) + 1));
      console.log($("#f_nums").val());
      i = $("#followers").find('form').first();
      b = i.clone();
      b.find('input').each(function(index, el) {
        el = $(this);
        el.attr('id', el.attr('id') + $("#f_nums").val());
        el.next().attr('for', el.attr('id'));
      });
      $("#followers").append(b);
    }







    function getLoadUrl(){
      a = "place=" + $('#f_place').val() + "&";
      a += "from=" + window.timefrom + "&";
      a += "until=" + window.timeto + "&";
      a += "persons=" + $('#persons').val() + "&";
      a += "parent_phone=" + $('#parent-phone').val() + "&";
      a += "parent_name=" + $('#parent-name').val() + "&";
      a += "parent_code=" + $('#code-melli').val() + "&";
      num = parseInt($("#f_nums").val());
      if (num == 0){
        a += "person_phone=" + $("#f_parent-phone").val() + "&";
        a += "person_code=" + $("#f_code-melli").val() + "&";
        a += "person_name=" + $("#f_parent-name").val() + "&";
      }else{
        a += "person_phone=" + $("#f_parent-phone").val() + "&";
        a += "person_code=" + $("#f_code-melli").val() + "&";
        a += "person_name=" + $("#f_parent-name").val() + "&";
        for (i = 1; i <= num; i++){
          a += "person_phone" + i + "=" + $("#f_parent-phone" + i).val() + "&";
          a += "person_code" + i + "=" + $("#f_code-melli" + i).val() + "&";
          a += "person_name" + i + "=" + $("#f_parent-name" + i).val() + "&";
        }
      }
      console.log(a);
      return a;
    }



    function showPrompt(){
      md = $("#modal-promp");
      md.find('#hotel-name').html($("#f_place_title").val());
      md.find('#hotel-persons').html($('#persons').val());
      md.find('#hotel-from').html($("#date-from").val());
      md.find('#hotel-to').html($("#date-until").val());
      md.modal('open');
    }




    function showShopLevel(){
      md = $("#modal-watcher");
      md.find('#hotel-name').html($("#f_place_title").val());
      md.find('#hotel-persons').html($('#persons').val());
      md.find('#hotel-from').html($("#date-from").val());
      md.find('#hotel-to').html($("#date-until").val());
      md.find("#p-parent-name").html($("#parent-name").val());
      md.find("#p-parent-code").html($("#code-melli").val());
      md.find("#p-parent-phone").html($("#parent-phone").val());
      persons = parseInt($('#persons').val());
      price = parseInt(md.find('#hotel-name').find('.price').html());
      md.find('#hotel-price').html(toPersDigits("" + persons * price * window.timeduration));
      var a = $('<tr><td id="parent-name" class="right-align"></td><td id="parent-code" class="right-align"></td><td id="parent-phone" class="right-align"></td></tr>');
      $('#p-followers').html("");
      $("#followers form").each(function(index, el){
        el = $(el);
        a.find('#parent-name').html(el.find('.parent-name').val());
        a.find('#parent-phone').html(el.find('.parent-phone').val());
        a.find('#parent-code').html(el.find('.parent-code').val());
        $('#p-followers').append(a);
        a = a.clone();
      });
      md.modal('open');
    }




    function sendData(){
      $("#loader").fadeIn();
      url = '{{url('/')}}' + '/api/send?' + getLoadUrl();
      $.ajax({
        url: url
      }).done(function(e){
        Materialize.toast('درخواست شما ثبت شد!', 3000, 'green rounded')
        $("#modal1").modal('open');
      }).always(function(){
        $("#loader").fadeOut();
      });
    }



    function toPersDigits(e){
 var id= ['۰','۱','۲','۳','۴','۵','۶','۷','۸','۹'];
 return e.replace(/[0-9]/g, function(w){
  return id[+w];
 });
}






    </script>
  </body>
</html>
