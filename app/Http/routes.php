<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Config as Config;
use Zarinpal\Drivers\SoapDriver;
use Zarinpal\Zarinpal;


date_default_timezone_set("Asia/Tehran");
Route::get('/', function () {
    return view('main');
});

Route::get('new', function(){
  return view('new');
});

Route::get('rules', function(){
  return view('rules');
});


Route::get('api/places/get', 'SearchPlaces@Ajax_getPlaces');


Route::get('api/cities/{id}/places', 'SearchPlaces@Ajax_getPlacesOfCity');


Route::get('api/send', 'SearchPlaces@insertRequest');

Route::get('test/zarin', function(){
	$test = new Zarinpal(Config::get('Zarinpal.merchantID'),new SoapDriver());
echo json_encode($answer = $test->request("example.com/testVerify.php",1000,'testing'));
if(isset($answer['Authority'])) {
    file_put_contents('Authority',$answer['Authority']);
    var_dump($test->redirect());
}
});



Route::get('donate', function(Request $req){
	return view('majd');
});


