<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;


use App\Requests as Reqs;

class SearchPlaces extends Controller
{
    //


    public function Ajax_getPlaces(Request $req){
      $place = trim($req->input('place'));
      $persons = trim($req->input('persons'));
      $from = trim($req->input('from'));
      $to = trim($req->input('to'));
      if (!$req->exists('food'))
        return abort(500);
      if (!$req->exists('reserv'))
        return abort(500);
      $food = $req->input('food');
      $reserv = $req->input('reserv');
      if (empty($place) || $place == "null"){
        $place = "0";
      }
      if ($place == "0"){
        $arr = DB::table('places')->where('value', '>=', $persons)->get();
      }else{
        $arr = DB::table('places')->where('id', $place)->where('value', '>=', $persons)->get();
      }


      if ($food == "0"){
      }else if ($food == "1"){
        foreach ($arr as $key => $value) {
          $value->price = (int) $value->price + (int) $value->food_price;
        }
      }else{
        return abort(500);
      }


      if ($reserv == "0"){
      }else if ($reserv == "1"){
        foreach ($arr as $key => $value) {
          $value->price = (int) $value->price * (int) $value->value;
        }
      }else{
        return abort(500);
      }


      return $arr;

      //$from = MD::jDateToTime($from);
      //$to = MD::jDateToTime($to);
      /*$exc = array();
      if ($place == "0"){
        $req = DB::table('requests')->get();
        foreach ($req as $key => $value) {
          $df = $value->dateUntil;
          $dt = $value->dateTo;
          if ($df > $from && $dt > $to && )
            $exc[] = $value->place;
          else if ($df > $from && $dt < $to)
            $exc[] = $value->place;
          else if ($df < $from && $dt > $from)
            $exc[] = $value->place;
          else if ($df < $to && $dt > $to)
            $exc[] = $value->place;
        }
      }else{
        $req = DB::table('requests')->where('place', $place)->get();
        foreach ($req as $key => $value) {
          $df = $value->dateUntil;
          $dt = $value->dateTo;
          if ($df > $from && $dt > $to)
            $exc[] = $value->place;
          else if ($df > $from && $dt < $to)
            $exc[] = $value->place;
          else if ($df < $from && $dt > $from)
            $exc[] = $value->place;
          else if ($df < $to && $dt > $to)
            $exc[] = $value->place;
        }
      }


      return $exc;*/
    }





    public function Ajax_getPlacesOfCity($id){
      $q = DB::table('places')->select(['title', 'id'])->where('city', trim($id))->get();
      return $q;
    }




    public function insertRequest(Request $req){
      $place = $req->input('place');
      $from = $req->input('from');
      $until = $req->input('until');
      $reserv = $req->input('reserv');
      $persons = $req->input('persons');
      $parent_name = $req->input('parent_name');
      $parent_code = $req->input('parent_code');
      $parent_phone = $req->input('parent_phone');
      $persons = (int)$persons;

      $r = new Reqs;
      $r->place = $place;
      $r->persons = $persons;
      $r->dateUntil = $from;
      $r->dateTo = $until;
      $r->paied = 1;
      $r->save();

      $title = DB::table('places')->where('id', $place)->first();
      //$city = DB::table('cities')->where('id', $title->city)->first()->title;
      $title = $title->title;
      Sms::send_msg(trim($parent_phone), "جناب " . $parent_name . "\n" . "درخواست شما با کد پیگیری " . $r->id . " برای " . $persons . "نفر از تاریخ " . $from . " تا تاریخ " . $until . " در " . $title . " ثبت شد");

      DB::table('parents')->insert([
        'req_id' => $r->id,
        'name' => $parent_name,
        'phone' => $parent_phone,
        'national_code' => $parent_code
        ]);


      $persons_name = $req->input('person_name');
      $persons_code = $req->input('person_code');
      $persons_phone = $req->input('person_phone');
      if ($persons_name != null && !empty($persons_name)) DB::table('followers')->insert([
        'req_id' => $r->id,
        'name' => $persons_name,
        'phone' => $persons_phone,
        'national_code' => $persons_code
        ]);

      if ($persons > 1){
        for ($i = 1; $i <= $persons; $i++){
          $persons_name = $req->input('person_name' . $i);
          $persons_code = $req->input('person_code' . $i);
          $persons_phone = $req->input('person_phone' . $i);
          if ($persons_name != null && !empty($persons_name))
          DB::table('followers')->insert([
            'req_id' => $r->id,
            'name' => $persons_name,
            'phone' => $persons_phone,
            'national_code' => $persons_code
          ]);
          else
            continue;
        }
      }

      return response("done", 200);
    }
}
