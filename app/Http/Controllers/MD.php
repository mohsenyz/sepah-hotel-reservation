<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use jDateTime;

use jDate;
class MD extends Controller
{
    //

    public static function jDateToTime($s){
    	$arr = explode("-", $s);
		$dateTime = jDateTime::toGregorian($arr[0], $arr[1], $arr[2]);
		return jDate::forge(implode("-", $dateTime))->time();
    }


    public static function getDayLength($e){
    	return (e + (24 * 60 * 60));
    }
}
